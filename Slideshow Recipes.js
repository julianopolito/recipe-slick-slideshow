(function() {

    /**
     * @function buildBindId
     * @param {Object} context
     * @description Create unique bind ID based on the campaign and experience IDs.
     */
    function buildBindId(context) {
        return `${context.campaign}:${context.experience}`;
    }

    function apply(context, template) {
        const contentZoneSelector = Evergage.getContentZoneSelector(context.contentZone);
        console.log("carousel prods: ", context.products);
        /**
         * The pageElementLoaded method waits for the content zone to load into the DOM
         * before rendering the template. The observer element that monitors for the content
         * zone element to get inserted into its DOM node is set to "body" by default.
         * For performance optimization, this default can be overridden by adding
         * a second selector argument, which will be used as the observer element instead.
         *
         * Visit the Template Display Utilities documentation to learn more:
         * https://developer.evergage.com/campaign-development/web-templates/web-display-utilities
         */

        return Evergage.DisplayUtils
            .bind(buildBindId(context))
            .pageElementLoaded(contentZoneSelector)
            .then((element) => {
                var script = document.createElement("LINK");
                script.id = "mycss";
                script.href = '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css';
                script.type = 'text/css';
                script.rel = "stylesheet";
                script.async = false;
                document.getElementsByTagName("head")[0].appendChild(script);

                script = document.createElement("LINK");
                script.id = "mycss2";
                script.href = '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css';
                script.type = 'text/css';
                script.rel = "stylesheet";
                script.async = false;
                document.getElementsByTagName("head")[0].appendChild(script);

                script = document.createElement("SCRIPT");
                script.src = '//code.jquery.com/jquery-1.11.0.min.js';
                script.type = 'text/javascript';
                script.async = false;
                document.getElementsByTagName("head")[0].appendChild(script);

                script = document.createElement("SCRIPT");
                script.src = '//code.jquery.com/jquery-migrate-1.2.1.min.js';
                script.type = 'text/javascript';
                script.async = false;
                document.getElementsByTagName("head")[0].appendChild(script);

                script = document.createElement("SCRIPT");
                script.src = '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js';
                script.type = 'text/javascript';
                script.async = false;
                document.getElementsByTagName("head")[0].appendChild(script);
                

                Evergage.DisplayUtils
                .bind(buildBindId(context))
                .pageElementLoaded("#mycss").then(function(){
                    const html = template(context);
                    Evergage.cashDom(element).html(html);
                    setTimeout(function(){
                        var jp = jQuery.noConflict();
                        document.documentElement.style.setProperty('--slide-bg-color',context.slideBgColor.hex);
                        document.documentElement.style.setProperty('--slide-text-color',context.slideTextColor.hex);
                        var options = {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            autoplay: true,
                            autoplaySpeed: context.autoplaySpeed,
                            dots:true
                        };
                        if(context.transition == 'fade'){
                            options.fade = true;
                        }
                        jp('.sftpl-is-slideshow7').slick(options);
                        jp('.sftpl-is-slideshow7').show();
                    }, 300);

                    console.log("returning template ", element, html);
                });
            }); 
        
            /*Evergage.cashDom(".carControl").first().trigger("click");
            document.documentElement.style.setProperty('--prod-bg-color',context.prodBgColor.hex);
            var pc = context.prodBgColor;
            var luminance = 0.2126*pc.r + 0.7152*pc.g + 0.0722*pc.b;
            if(luminance > 150){
                document.documentElement.style.setProperty('--button-text-color',"#000");
            }else{
                document.documentElement.style.setProperty('--button-text-color',"#fff");
            }
            console.log("luminance ", luminance);
            */
    }

    function reset(context, template) {
        Evergage.DisplayUtils.unbind(buildBindId(context));
        Evergage.cashDom(".sftpl-is-slideshow7").remove();
    }

    function control(context) {
        
    }

    registerTemplate({
        apply: apply,
        reset: reset,
        control: control
    });

})();
