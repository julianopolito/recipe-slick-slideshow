import { RecommendationsConfig, recommend } from "recs";

export class BannerWithCTATemplate implements CampaignTemplateComponent {
    /**
     * Developer Controls
     */

    maximumNumberOfProducts = 6;
    @title('Autoplay Speed (ms)')
    autoplaySpeed = 1500;

    @options([
        "fade",
        "slide"
    ])
    transition: String = "slide";

    /**
     * Business-User Controls
     */

    @title(" ")
    recsConfig: RecommendationsConfig = new RecommendationsConfig()
        .restrictItemType("Product")
        .restrictMaxResults(this.maximumNumberOfProducts);

    @header("Customization Options")

    slideTextColor:Color;
    slideBgColor:Color;

    run(context: CampaignComponentContext) {
        this.recsConfig.maxResults = this.maximumNumberOfProducts;
        return { 
            rec: recommend(context, this.recsConfig),
            itemType: this.recsConfig.itemType
        };
    }

}


